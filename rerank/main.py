from settings import settings
from evaluator import Evaluator
import pandas as pd
import time


def main():
    measurements = settings.measurements
    fields = settings.search_fields
    df = pd.DataFrame(columns=measurements)
    rf = pd.DataFrame(columns=measurements)
    evaluator = Evaluator(
        settings.host,
        settings.port,
        settings.index,
        measurements,
        settings.return_bm25,
        settings.relevance_path,
        settings.topics_path,
    )
    start = time.time()
    for field in fields:
        print(field)
        t, r = evaluator.eval(field)
        df = df.append(t)
        rf = rf.append(r)
    end = time.time()
    df.to_csv("nist_results.csv")
    rf.to_csv(settings.save_results_path)
    print("time: " + str(end - start))


main()
