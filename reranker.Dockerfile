FROM  nvidia/cuda:10.0-base

ARG DEBIAN_FRONTEND=noninteractive

ENV CUDNN_VERSION 7.4.2.24
ENV PYTHON_VERSION="3.7.4"

WORKDIR /base

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        dpkg-dev \
        gcc \
        curl \
        vim \
        g++ \
        git    \
        libbz2-dev \
        libc6-dev \
        libcudnn7=${CUDNN_VERSION}-1+cuda10.0 \
        libcudnn7-dev=${CUDNN_VERSION}-1+cuda10.0 \
    cuda-cublas-10-0 \
        cuda-curand-10-0 \
        cuda-npp-10-0 \
    cuda-cufft-10-0 \
    cuda-cusparse-10-0 \
    cuda-cusolver-10-0 \
        libexpat1-dev \
        libffi-dev \
        libgdbm-dev \
        liblzma-dev \
        libncursesw5-dev \
        libreadline-dev \
        libsqlite3-dev \
        libssl-dev \
        make \
        tk-dev \
        wget \
        xz-utils \
        unzip \
        zlib1g-dev && \
    wget --no-check-certificate -O python.tar.xz https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz && \
    mkdir -p /usr/src/python && \
    tar -xC /usr/src/python --strip-components=1 -f python.tar.xz && \
    rm python.tar.xz && \
    cd /usr/src/python && \
    ./configure && \
    make -j "$(nproc)" altinstall && \
    ln -s /usr/local/bin/python${PYTHON_VERSION%.*} /usr/local/bin/python && \
    ln -s /usr/local/bin/pip${PYTHON_VERSION%.*} /usr/local/bin/pip && \
    pip install torch==1.2.0 \
                transformers \
                pytrec_eval \
                numpy \
                pandas \
                elasticsearch \
                tensorflow==2.0 \
                pydantic \
                gsutil
                
RUN gsutil cp -r gs://neuralresearcher_data/covid/data/model_exp304 .

RUN curl https://raw.githubusercontent.com/castorini/anserini/master/src/main/resources/topics-and-qrels/qrels.dl19-doc.txt --output qrels.dl19-doc.txt
RUN curl https://raw.githubusercontent.com/castorini/anserini/master/src/main/resources/topics-and-qrels/topics.dl19-doc.txt --output topics.dl19-doc.txt

                
    

