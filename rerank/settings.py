from pathlib import Path
import os

from pydantic import BaseSettings


class Settings(BaseSettings):

    # T5 model settings
    t5_model_dir: str = "model_exp304"
    t5_batch_size: int = 6
    t5_device: str = "cuda:0"
    t5_model_type: str = "t5-base"
    t5_max_length: int = 512

    # Cache settings
    cache_dir: Path = Path(
        os.getenv("XDG_CACHE_HOME", str(Path.home() / ".cache"))
    ) / "trec"
    flush_cache: bool = False
    # Elasticsearch Index
    host: str = "ha-opendistro-es-discovery.opendistro"
    port: int = 9200

    # Measurements / Retrieval
    measurements: list = [
        "map",
        "ndcg_cut_10",
        "ndcg_cut_100",
        "recall_10",
        "recall_100",
        "recall_1000",
        "recip_rank",
    ]
    search_fields: list = [["text"]]
    save_results_path: str = "text_reranked.csv"
    return_bm25: bool = True
    index: str = "trec_text_based_documents"
    topics_path: str = "topics.dl19-doc.txt"
    relevance_path: str = "qrels.dl19-doc.txt"


settings = Settings()
