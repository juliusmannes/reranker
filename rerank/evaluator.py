from elasticsearch import Elasticsearch
from ranker import Ranker
import pytrec_eval
import pandas as pd

"""Class that retrieves and evaluates results.
"""

class Evaluator:
    def __init__(
    """Constructor.
    Arguments:
        host: String = host adress of Elasticsearch instance
        port: Int = port of Elasticsearch instance
        index: String = Index to search in
        measurements: List[String] = Measurements to be taken and evaluated (Precision, Recall..)
        return_bm25: bool = If results for BM25 should be returned
        relevance_path: String = Path to the stored relevance judgements
        topics_path: String =  Path to the stored topics.
    """
        self, host, port, index, measurements, return_bm25, relevance_path, topics_path
    ):
        self.__client = Elasticsearch([{"host": host, "port": port}])
        self.__index = index
        self.__measurements = measurements
        self.__return_bm25 = return_bm25
        self.__relevance_path = relevance_path
        self.__topics_path = topics_path
        self.__relevance_judgements = self.text_rels()

    def text_rels(self):
    """Reads relevance judgements and returns them as a Dict.
    """
        qrel = {}
        with open(self.__relevance_path, "r") as f:
            lines = [line.rstrip("\n") for line in f]
            for line in lines:
                split = line.strip().split()
                topic = split[0]
                docid = split[2]
                rel = split[3]
                if topic not in qrel:
                    qrel[topic] = {}
                qrel[topic][docid] = int(rel)
        return qrel

    def empty_measurements(self):
    """Returns a Dict of the required measurements initialized at 0.0
    """
        metrics = {}
        for m in self.__measurements:
            metrics[m] = 0.0
        return metrics

    def evaluate_results(self, search_results, evaluator, fields):
    """Returns a DataFrame containing the results for each requested measurement
    on the requested search fields.
    
    Arguments:
        search_results: Dict = ID's with associated scores
        evaluator: pytrec_eval Object = initialized on self.__relevance_judgements and all supported measures
        fields: List[String] = Which fields were used for querying.
    """
        results = evaluator.evaluate(search_results)
        metrics = self.empty_measurements()
        for r in results:
            for metric in metrics:
                metrics[metric] += results[r][metric]
        means = []
        means.append(fields)
        for metric, value in metrics.items():
            means.append(float(value) / float(len(results)))
        result_df = pd.DataFrame(data=[means], columns=["Fields"] + self.__measurements)
        return result_df

    def eval(self, fields):
    """Collects search results and evaluates them
        
        Arguments:
            fields: List[String] = Which fields to query in the index.
    """
        bm25, reranked = self.read_dl_topics(fields)
        evaluator = pytrec_eval.RelevanceEvaluator(
            self.__relevance_judgements, pytrec_eval.supported_measures
        )
        reranked_results = self.evaluate_results(reranked, evaluator, fields)

        if self.__return_bm25:
            bm_results = self.evaluate_results(bm25, evaluator, fields)
            return reranked_results, bm_results

        return reranked_results
        
    def rerank(self,results):
    """Splits each document in the results into paragraphs. Assigns each paragraph a score,
        using the T5 model. The maximum score is assigned to the document. Returns a dict of
        document ids with associated scores.
        
        Arguments:
            results: Dict = results from an elasticsearch query with ['_source'] returned.
    """
        ids = [id["_id"] for id in results["hits"]["hits"]]
        t5_scores = []
        for hit in results["hits"]["hits"]:
            paragraphs = []
            for i in hit["_source"]["text"].split("\n"):
                paragraphs.append(i[:5000])
            t5_inputs = [
                f"Query: {q} Document: {p} Relevant:" for p in paragraphs
            ]
            scores = ranker.predict_t5(t5_inputs)
            t5_scores.append(max(scores))
        
        return dict(zip(ids, t5_scores))
        
    def read_dl_topics(self, fields, size=1000):
    """Queries the index on the given fields. The resulting documents are split into paragraphs,
        which are given a score from the T5 model. The overall score of a document equals the
        maximum score of the paragraphs of that document. The ID's of the retrieved documents
        with associated scores are returned as a Dict. Optionally, this Dict for BM25 results is
        returned as well.
        
        Arguments:
            fields: List[String] = Which fields to query
            size: Int = How many results to return (Default is 1000)
    """
        results = {}
        reranked = {}
        ranker = Ranker()
        with open(self.__topics_path, "r") as f:
            lines = [line.rstrip("\n") for line in f]
            for line in lines:
                split = line.strip().split("\t")
                topic_id = split[0]
                q = split[1]
                print(q)
                query = {"query": {"multi_match": {"query": q, "fields": fields}}}
                res = self.__client.search(
                    size=size,
                    index=self.__index,
                    body=query,
                    _source=True,
                    request_timeout=600000,
                )
                reranked[topic_id] = rerank(res)
                if self.__return_bm25:
                    results[topic_id] = {}
                    for hit in res["hits"]["hits"]:
                        results[topic_id][hit["_id"]] = hit["_score"]

        return results, reranked
